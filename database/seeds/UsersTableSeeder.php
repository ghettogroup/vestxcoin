<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\User::create(array(
            'name' => "GG-Valtheim",
            'email' => "valtheim@admin.com",
            'password' => Hash::make('123123123')
        ));
        \App\User::create(array(
            'name' => "GG-Ryan",
            'email' => "ryan@admin.com",
            'password' => Hash::make('123123123')
        ));
    }
}
