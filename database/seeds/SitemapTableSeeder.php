<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\Sitemap;

class SitemapTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('sitemaps')->delete();

		Sitemap::create(array(
				'name' => "About"
			));

		Sitemap::create(array(
				'name' => "Tools"
			));

		Sitemap::create(array(
				'name' => "Features"
			));

		Sitemap::create(array(
				'name' => "Blockchain Works"
			));

		Sitemap::create(array(
				'name' => "Best Teams"
			));

		Sitemap::create(array(
				'name' => "Roadmaps"
			));
		Sitemap::create(array(
				'name' => "Social Media"
			));

	}
}