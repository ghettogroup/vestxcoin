<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Model::unguard();

		$this->call('SitemapTableSeeder');
		$this->call('UsersTableSeeder');
		$this->command->info('Sitemap table seeded!');
	}
}