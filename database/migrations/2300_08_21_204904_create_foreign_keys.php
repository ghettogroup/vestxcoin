<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('contents', function(Blueprint $table) {
			$table->foreign('sitemap_id')->references('id')->on('sitemaps')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('pages', function(Blueprint $table) {
			$table->foreign('sitemap_id')->references('id')->on('sitemaps')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('page_details', function(Blueprint $table) {
			$table->foreign('page_id')->references('id')->on('pages')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('contents', function(Blueprint $table) {
			$table->dropForeign('contents_sitemap_id_foreign');
		});
		Schema::table('pages', function(Blueprint $table) {
			$table->dropForeign('pages_sitemap_id_foreign');
		});
		Schema::table('page_details', function(Blueprint $table) {
			$table->dropForeign('page_details_page_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_page_id_foreign');
		});
        Schema::table('form_details', function(Blueprint $table) {
            $table->dropForeign('form_details_form_id_foreign');
        });
	}
}