<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageDetailsTable extends Migration {

	public function up()
	{
		Schema::create('page_details', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('page_id')->unsigned();
			$table->string('name', 255);
			$table->string('slug')->unique()->nullable();
			$table->text('detail')->nullable();
			$table->string('custom_text_1', 255)->nullable();
			$table->string('custom_text_2', 255)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('page_details');
	}
}