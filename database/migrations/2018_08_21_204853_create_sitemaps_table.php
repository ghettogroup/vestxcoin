<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitemapsTable extends Migration {

	public function up()
	{
		Schema::create('sitemaps', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->tinyInteger('status')->unsigned()->default('1');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sitemaps');
	}
}