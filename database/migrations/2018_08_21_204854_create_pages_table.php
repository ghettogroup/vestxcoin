<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('sitemap_id')->unsigned();
			$table->tinyInteger('status')->unsigned()->default('1');
			$table->string('image', 255)->nullable();
			$table->integer('views')->default('0');
			$table->string('custom_integer_1')->nullable();
			$table->string('custom_integer_2')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('pages');
	}
}