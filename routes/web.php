<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::middleware(['auth'])->prefix('admin')->group(function () {

    Route::prefix('/about')->group(function () {
        Route::get('/', 'Admin\ContentController@index');
        Route::post('/edit', 'Admin\ContentController@edit');
    });
    Route::prefix('/tools')->group(function () {
        Route::get('/', 'Admin\ContentController@index');
        Route::post('/edit', 'Admin\ContentController@edit');
    });

    Route::prefix('/social_media')->group(function () {
        Route::get('/', 'Admin\ContentController@index');
        Route::post('/edit', 'Admin\ContentController@edit');
    });

    Route::prefix('/features')->group(function () {
        Route::get('/', 'Admin\PageController@index');

        Route::prefix('/new')->group(function () {
            Route::get('/', 'Admin\PageDetailController@create');
            Route::post('/add', 'Admin\PageDetailController@store');
        });
        Route::prefix('/{id}')->group(function () {
            Route::get('/', 'Admin\PageDetailController@update');
            Route::post('/update', 'Admin\PageDetailController@edit');
            Route::get('/delete', 'Admin\PageDetailController@destroy');
        });
    });
    Route::prefix('/blockchain-works')->group(function () {
        Route::get('/', 'Admin\PageController@index');

        Route::prefix('/new')->group(function () {
            Route::get('/', 'Admin\PageDetailController@create');
            Route::post('/add', 'Admin\PageDetailController@store');
        });
        Route::prefix('/{id}')->group(function () {
            Route::get('/', 'Admin\PageDetailController@update');
            Route::post('/update', 'Admin\PageDetailController@edit');
            Route::get('/delete', 'Admin\PageDetailController@destroy');
        });
    });
    Route::prefix('/team')->group(function () {
        Route::get('/', 'Admin\PageController@index');

        Route::prefix('/new')->group(function () {
            Route::get('/', 'Admin\PageDetailController@create');
            Route::post('/add', 'Admin\PageDetailController@store');
        });
        Route::prefix('/{id}')->group(function () {
            Route::get('/', 'Admin\PageDetailController@update');
            Route::post('/update', 'Admin\PageDetailController@edit');
            Route::get('/delete', 'Admin\PageDetailController@destroy');
        });
    });

    Route::prefix('/roadmap')->group(function () {
        Route::get('/', 'Admin\PageController@index');

        Route::prefix('/new')->group(function () {
            Route::get('/', 'Admin\PageDetailController@create');
            Route::post('/add', 'Admin\PageDetailController@store');
        });
        Route::prefix('/{id}')->group(function () {
            Route::get('/', 'Admin\PageDetailController@update');
            Route::post('/update', 'Admin\PageDetailController@edit');
            Route::get('/delete', 'Admin\PageDetailController@destroy');
        });
    });


    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        echo "<p style='font-family: Roboto:700, serif'><font size='5'> Cache Temizlendi</font></p>";
    });
});

Route::get('/admin/', 'Admin\AdminLoginController@index')->name('login');
Route::post('/admin/', 'Admin\AdminLoginController@doLogin');