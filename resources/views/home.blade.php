<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <title>VestXcoin Landing Page</title>

    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">

    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/vendor/bicon/css/bicon.min.css" rel="stylesheet">
    <link href="assets/vendor/animate.css" rel="stylesheet">
    <link href="assets/vendor/owl/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="assets/css/v-effect.css" rel="stylesheet">

</head>
<body class="">
@php
    $content = $data['content'];
    $social = $data['social'];
    $features = $data['features'];
    $blockchain = $data['blockchain-works'];
    $team = $data['team'];
    $roadmap = $data['roadmap']->chunk(2);
@endphp
<header class="app-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-md" id="mainNav">

                    <a class="navbar-brand mr-5" href="#">
                        <img class="" src="assets/img/logo.png" srcset="assets/img/logo@2x.png 2x" alt=""/>
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                            <i class="fa fa-bars"> </i>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#about">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#tools">Tools</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#whyhow">Why & How</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#team">Team</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#login">Login</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Blog
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                    <a class="dropdown-item" href="blog-list.html">Blog Post</a>
                                    <a class="dropdown-item" href="blog-single.html">Blog Post Single</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link btn btn-pill buy-token js-scroll-trigger" href="#buytoken">Buy
                                    token</a>
                            </li>
                        </ul>
                    </div>

                </nav>
            </div>
        </div>
    </div>
</header>

<div class="hero-banner" id="home">
    <div class="banner-img">
        <div class="grid">
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
            <div class="child">V</div>
        </div>
    </div>
    <div class="hero-text">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h1 class="hero-title wow fadeInUp" data-wow-delay=".2s">Decentralized Anonymous Fast </h1>
                    <p class="here-sub-title wow fadeInUp" data-wow-delay=".3s">
                        Masternode Coin With Promising Features.

                    </p>

                    <a href="#" class="btn btn-gradient btn-pill text-uppercase wow fadeInUp" data-wow-delay=".4s">Get
                        Notified</a>
                    <div class="mt-md-5 mt-3 sm-txt gray-txt wow fadeInUp" data-wow-delay=".5s">
                        <div class="row">
                            @if(getArrayData($social, 'telegram'))
                                <div class="col-md-3"><a href="{!! $social['telegram'] !!}"><img src="assets/img/TelegramSocial.png" alt=""></a></div>
                            @endif
                            @if(getArrayData($social, 'twitter'))
                                <div class="col-md-3"><a href="{!! $social['twitter'] !!}"><img src="assets/img/TwitterSocial.png" alt=""></a></div>
                            @endif
                            @if(getArrayData($social, 'discord'))
                                <div class="col-md-3"><a href="{!! $social['discord'] !!}"><img src="assets/img/DiscordSocial.png" alt=""></a></div>
                            @endif
                            @if(getArrayData($social, 'facebook'))
                                <div class="col-md-3"><a href="{!! $social['facebook'] !!}"><img src="assets/img/FacebookSocial.png" alt=""></a></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="demo" id="section07">
            <a href="#about">
                <span></span>
                <span></span>
                <span></span>
                <div>Scroll</div>
            </a>
        </div>
    </div>
</div>

<div id="about">
    <section class="section-gap section-dark-bg">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="wow fadeInUp" data-wow-delay=".1s">{!! getArrayData($content, 'about_title') !!}</h2></div>
                    <a href="{!! getArrayData($content, 'about_white_paper') !!}" target="_blank"
                       class="btn btn-gradient btn-pill text-uppercase wow fadeInUp"
                       data-wow-delay=".25s">read whitepaper</a>
                </div>
            </div><!--section title-->

            <div class="row mt-lg-5 mt-3 pt-lg-5 mt-md-5">
                <div class="col-md-6 col-gap mb-md-0 mb-3">
                    <div class="card text-center py-md-3 px-md-4 wow fadeInUp" data-wow-delay=".2s">
                        <div class="card-body">
                            {!! getArrayData($content, 'about_detail') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-gap">
                    <div class="card text-center py-md-3 px-md-4 wow fadeInUp" data-wow-delay=".3s"
                         style="height:100%;">
                        <div class="card-body" id="video">
                            <div>
                                <video muted="" loop="" autoplay="" width="100%"
                                       src="assets/video/video.mp4"></video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<!--section end-->

<div id="tools">
    <!--section start-->
    <section class="section-gap pb-0">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 col-gap mb-md-0 mb-3">
                    <div class="wow fadeInUp" data-wow-delay=".2s">
                        <img src="{!! getArrayData($content, 'tools_upper_image') !!}" alt=""/>
                    </div>
                </div>
                <div class="col-md-6 col-gap">
                    <h2 class="red-title mb-md-5 wow fadeInUp" data-wow-delay=".3s">STATEMENT OF THE PROBLEM
                    </h2>
                    <p class="wow fadeInUp" data-wow-delay=".35s">
                        {!! getArrayData($content, 'tools_upper_detail') !!}
                    </p>
                </div>
                <div class="col-md-6 col-gap">
                    <p class="wow fadeInUp" data-wow-delay=".35s">
                        {!! getArrayData($content, 'tools_bottom_detail') !!}
                    </p>
                </div>
                <div class="col-md-6 col-gap mb-md-0 mb-3">
                    <div class="wow fadeInUp" data-wow-delay=".2s">
                        <img class="scale-anime" src="{!! getArrayData($content, 'tools_bottom_image') !!}" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>


<div id="whyhow">

    <section class="section-gap feature-gradient">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <div class="section-title wow fadeInUp" data-wow-delay=".2s">
                        <h3>VestXcoin Features</h3>
                        <h4 class="p-2">VestXcoin will have the following specific features They include
                        </h4>
                    </div>
                </div>
                <div class="row">
                    @foreach($features as $feature)
                        <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                            <div class="lds-hourglass"></div>
                            <i class="bi bi-{!! $feature->detail->custom_text_1 !!} f-icon"></i>
                            <h4 class="my-4">{!! $feature->detail->name !!}
                            </h4>
                            <p>{!! $feature->detail->detail !!}</p>
                        </div>
                    @endforeach
                </div>
            </div>
    </section>
    <!--section end-->

    <!--section start-->
    <section class="section-gap">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 col-gap wow fadeInUp" data-wow-delay=".5s">
                    <h2 class="mb-md-5 wow fadeInUp" data-wow-delay=".2s">Coin Specifications

                    </h2>
                    <img class="py-4" id="specs" src="assets/img/coinspecs.gif" alt=""/>
                </div>
            </div>
        </div>
    </section>
    <!--section end-->

    <!--section start-->
    <section class="section-gap section-dark-bg">
        <div class="container">
            <!--section title-->
            <div class="row text-center">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class=" wow fadeInUp" data-wow-delay=".2s">How Blockchain Works</h2>
                        <p class=" wow fadeInUp" data-wow-delay=".3s"> to find the ones who get it right. We trust our
                            future with experts everyday</p>
                    </div>
                </div>
            </div><!--section title-->
            <div class="row d-flex align-items-center wow fadeInUp" data-wow-delay=".4s">
                <div class="col-md-12 col-gap">
                    <div class="owl-carousel owl-theme js_steps_carousel">
                        @foreach($blockchain as $value)
                            <div class="row">
                                <div class="col-md-12" data-dot="<i class='bi bi-{!! $value->detail->custom_text_1 !!}'></i>">
                                    <div class="steps-content">
                                        <h3 class="mb-md-3">{!! $value->detail->name !!}</h3>
                                        <p>{!! $value->detail->detail !!}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--section end-->
</div>


<div id="team">
    <!--team section start-->
    <section class="section-gap pb-0">
        <div class="container">
            <!--section title-->
            <div class="row text-center">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="gray-title wow fadeInUp" data-wow-delay=".2s">We Have a Best Team</h2>
                        <p class=" wow fadeInUp" data-wow-delay=".3s"> to find the ones who get it right. We trust our
                            future with experts everyday</p>
                    </div>
                </div>
            </div><!--section title-->
            <div class="row text-md-left text-center">
                @foreach($team as $value)
                    <div class="col-md-4 wow fadeInUp" data-wow-delay=".4s">
                        <img class="rounded mb-md-0 mb-3" src="{!! $value->image !!}" alt=""/>
                        <h6 class="mt-md-4 mb-0">{!! $value->detail->name !!}</h6>
                        <p class="sm-txt">{!! $value->detail->detail !!}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--team section end-->
</div>

<div id="faq">
    <!--faq section start-->
    <section class="section-gap">
        <div class="container">
            <!--section title-->
            <div class="row text-center">
                <div class="col-12">
                    <div class="section-title">
                        <h2 class="red-title wow fadeInUp" data-wow-delay=".2s">ROADMAP</h2>
                        <p class=" wow fadeInUp" data-wow-delay=".3s">When looking at its layout. The point of using
                            Lorem Ipsum <br/>
                            is that it has a more-or-less normal distribution</p>
                    </div>
                </div>
            </div>
        </div><!--section title-->
        <div class="roadmap container-fluid">
            <div class="row">
                @foreach($roadmap as $value)
                    <div class="col-md-6 col-gap">
                        <div class="accordion wow fadeInUp" data-wow-delay=".4s" id="accordion-{!! $loop->iteration !!}">
                            @foreach($value as $data)
                            <div class="card">
                                <div class="card-header">
                                    <h5>
                                        <a class="collapsed" data-toggle="collapse" data-target="#collapse-{!! $data->id !!}">
                                            {!! $data->detail->name !!}
                                        </a>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar"
                                                 aria-valuenow="{!! $data->custom_integer_1 !!}"
                                                 aria-valuemin="0" aria-valuemax="100" style="width:{!! $data->custom_integer_1 !!}%">
                                                {!! $data->custom_integer_1 !!}% Complete
                                            </div>
                                        </div>
                                    </h5>
                                </div>
                                <div id="collapse-{!! $data->id !!}" class="collapse" data-parent="#accordion-{!! $loop->parent->iteration !!}">
                                    <div class="card-body pt-0">
                                        <p>{!! $data->detail->detail !!}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
            @endforeach
            </div>
        </div>
    </section>
</div>
<!--faq section end-->
</div>

<div id="login" class=" wow fadeInUp" data-wow-delay=".2s">
    <!--registration section start-->
    <section class="signin-row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:;" class="registration-bg" data-toggle="modal" data-target="#registerModal">
                        <span class="title">Register</span>
                        <p>Join the new era of cryptocurrency exchange</p>
                    </a>
                    <!-- register modal -->
                    <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="row no-gutters">
                                    <div class="col-md-5">
                                        <img class="login-banner-img" src="assets/img/login-img.jpg" alt=""/>
                                    </div>
                                    <div class="col-md-7">
                                        <!--register form-->
                                        <div class="login-form p-5">
                                            <h4 class="text-uppercase mt-0 text-purple-color text-center mb-5">
                                                Register</h4>
                                            <form>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="inputUser10"
                                                           placeholder="Username / email / phone">
                                                </div>
                                                <div class="form-group mb-4">
                                                    <input type="password" class="form-control" id="InputPassword10"
                                                           placeholder="Password">
                                                </div>
                                                <div class="form-group mb-4">
                                                    <input type="password" class="form-control" id="confirmPassword10"
                                                           placeholder="Confirm Password">
                                                </div>

                                                <div class="form-group clearfix">
                                                    <button type="submit" class="btn btn-purple float-right">Register
                                                    </button>
                                                </div>

                                                <div class="text-center mt-5">
                                                    Already have an account? <a href="#"
                                                                                class="text-purple-color text-capitalize f12">Login</a>
                                                </div>
                                            </form>
                                        </div>
                                        <!--/register form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!--register modal-->

                    <a href="javascript:;" class="login-bg" data-toggle="modal" data-target="#loginModal">
                        <span class="title">Sign In</span>
                        <p>Access the cryptocurrency experience you deserve</p>
                    </a>

                    <!-- login modal -->
                    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img class="login-banner-img" src="assets/img/login-img.jpg" alt=""/>
                                    </div>
                                    <div class="col-md-7">
                                        <!--login form-->
                                        <div class="login-form p-5">
                                            <h4 class="text-uppercase mt-0 text-purple-color text-center mb-5">
                                                Login</h4>
                                            <form>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                                           placeholder="Enter email">
                                                </div>
                                                <div class="form-group mb-4">
                                                    <input type="password" class="form-control"
                                                           id="exampleInputPassword1" placeholder="Enter Password">
                                                </div>

                                                <div class="form-group clearfix">
                                                    <a href="#" class="float-left forgot-link my-2">Forgot Password
                                                        ?</a>
                                                    <button type="submit" class="btn btn-purple float-right">LOGIN
                                                    </button>
                                                </div>

                                                <div class="text-center mt-5">
                                                    <a href="#" class="text-purple-color text-capitalize f12">Create New
                                                        Account</a>
                                                </div>
                                            </form>
                                        </div>
                                        <!--/login form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!--login modal-->
                </div>
            </div>
        </div>
    </section>
    <!--registration section end-->
</div>

<!--footer start-->
<footer class="app-footer">
    <div class="primary-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 wow fadeInUp" data-wow-delay=".2s">
                    <h4 class="text-uppercase">VestXcoin</h4>
                    <p>Subscribe to our newsletter to receive news, updates, free stuff and new releases by email. We
                        don't do spam.</p>
                    <p class="copyright">&copy; Copyright 2018 VectorLab, Inc.™</p>
                </div>
                <div class="col-md-4 mb-md-0 mb-3 wow fadeInUp" data-wow-delay=".3s">
                    <h6 class="text-uppercase">Resources</h6>
                    <ul class="list-unstyled">
                        <li><a href="javascript:;">Free schedules</a></li>
                        <li><a href="javascript:;">terms & privacy</a></li>
                        <li><a href="javascript:;">api reference</a></li>
                        <li><a href="javascript:;">help desk</a></li>
                    </ul>
                </div>
                <div class="col-md-4 wow fadeInUp" data-wow-delay=".4s">
                    <h6 class="text-uppercase">About & Contacts</h6>
                    <ul class="list-unstyled">
                        <li><a href="javascript:;">Our Vision</a></li>
                        <li><a href="javascript:;">Features</a></li>
                        <li><a href="javascript:;">About Us</a></li>
                        <li><a href="javascript:;">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="secondary-footer text-md-left text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-md-0 mb-3">
                    <div class="payment-cards wow fadeInUp" data-wow-delay=".45s">
                        <img src="assets/img/logo.png" style="filter: brightness(0%);" alt=""/>

                    </div>
                </div>
                <div class="col-md-6 text-md-right wow fadeInUp" data-wow-delay=".5s">
                    <div class="float-md-right  mb-md-0 mb-3">
                        <span class="sm-txt pr-2">Language:</span>
                        <div class="btn-group ">
                            <button type="button" class="btn-language dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                English
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="button">English</button>
                                <button class="dropdown-item" type="button">French</button>
                                <button class="dropdown-item" type="button">Chinese</button>
                            </div>
                        </div>
                    </div>

                    <div class="social-links float-md-right">
                        @foreach($social as $key=>$value)
                            <a href="{!! $value !!}"><i class="fa fa-{!! $key !!}"></i></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer end-->


<!--js initialized-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/popper.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/wow.min.js"></script>
<script src="assets/vendor/jquery.easing.min.js"></script>
<script src="assets/vendor/owl/owl.carousel.min.js"></script>
<script src="assets/vendor/jquery.countdown.min.js"></script>
<script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="assets/vendor/particles/particles.js"></script>
<script src="assets/vendor/particles/init-particles.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

<!--contact form-->
<script type="text/javascript" src="assets/vendor/contact-form/form-validator.min.js"></script>
<script type="text/javascript" src="assets/vendor/contact-form/contact-form-script.js"></script>

<!--init scripts-->
<script src="assets/js/scripts.js"></script>

</body>
</html>