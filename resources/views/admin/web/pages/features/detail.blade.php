@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/update', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">FEATURES</h3>

                        <div class="row">
                            <div class="col-sm-6 col-lg-8" style="margin: auto">
                                <div class="form-group">
                                    <label class="form-label">Icon Name</label>
                                    <input type="text" class="form-control" name="custom_text_1"
                                           placeholder="group, alarm-clock, brick-wall, pin-man-range"
                                           value="{!! $data->detail->custom_text_1 !!}" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Title</label>
                                    <input type="text" class="form-control" name="name"
                                           value="{!! $data->detail->name !!}" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Detail</label>
                                    <textarea class="form-control ckeditor" name="detail"
                                              rows="2" required>{!! $data->detail->detail !!}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                        <input type="hidden" value="3" name="sitemap_id">
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#IconBtn').on('click', function () {
            $('#features_icon').trigger('click');
        });
    </script>
@endpush