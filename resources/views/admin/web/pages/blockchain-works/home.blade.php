@extends('admin.inc.app')

@section('content')

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this content?
                </div>
                <div class="modal-footer">
                    <a href="{!! app('request')->url()."/delete" !!}" class="btn btn-danger delete-features-href">Yes</a>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-11">
                                <h3 class="card-title">Blockchain Works</h3>
                            </div>
                            <div class="col-lg-1">
                                <a href="/admin/blockchain-works/new" class="btn btn-info" style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Add New</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Detail</th>
                                </tr>
                                </thead>
                                @foreach($data as $value)
                                    <tr>
                                        <td> {!! $value->id !!}</td>
                                        <td> {!! $value->detail->name !!}</td>
                                        <td> {!! $value->detail->detail !!}</td>
                                        <td>
                                            <a href="/admin/blockchain-works/{!! $value->id !!}" class="btn btn-primary" style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Edit</a>

                                            <button type="button" class="btn btn-danger delete-features" data-toggle="modal"
                                                    data-id="{!! $value->id !!}" data-target="#deleteModal"
                                                    style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        $(document).on("click", ".delete-features", function () {
            var userId = $(this).data('id');
            $(".delete-features-href").attr('href', "{!! app('request')->url()."/" !!}" + userId + "/delete");
        });
    </script>
@endpush