@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/edit', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">TOOLS</h3>

                        <div class="row">
                            <div class="col-sm-6 col-lg-12">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                    <span id="upperFile" style="cursor: pointer;">
                                        <label><b>Upper Image</b></label>
                                        <img src="{!! isset($data['tools_upper_image'] ) ? $data['tools_upper_image']  : "https://via.placeholder.com/100"!!}"
                                             alt="banner-photo" class="card p-1"
                                             style="width: 40%; margin-left: auto; margin-right: auto">
                                    </span>
                                        <div class="form-group" style="display: none">
                                            <div class="custom-file ">
                                                <input type="file" class="form-control" id="upper-file"
                                                       name="tools_upper_image">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label class="form-label">Upper Detail</label>
                                        <textarea class="form-control ckeditor" name="tools_upper_detail"
                                                  rows="2" required>{!! getArrayData($data,'tools_upper_detail') !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>


                            <div class="col-sm-6 col-lg-12">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label class="form-label">Bottom Detail</label>
                                        <textarea class="form-control ckeditor" name="tools_bottom_detail"
                                                  rows="2" required>{!! getArrayData($data,'tools_bottom_detail') !!}</textarea>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <span id="bottomFile" style="cursor: pointer;">
                                            <label><b>Bottom Image</b></label>
                                            <img src="{!! isset($data['tools_bottom_image'] ) ? $data['tools_bottom_image']  : "https://via.placeholder.com/100"!!}"
                                                 alt="banner-photo" class="card p-1"
                                                 style="width: 40%; margin-left: auto; margin-right: auto">
                                        </span>
                                        <div class="form-group" style="display: none">
                                            <div class="custom-file ">
                                                <input type="file" class="form-control" id="bottom-file"
                                                       name="tools_bottom_image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="2" name="sitemap_id">
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#bottomFile').on('click', function () {
            $('#bottom-file').trigger('click');
        });
        $('#upperFile').on('click', function () {
            $('#upper-file').trigger('click');
        });
    </script>
@endpush