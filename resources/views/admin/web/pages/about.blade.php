@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/edit', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">ABOUT US</h3>

                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Title</label>
                                    <input type="text" class="form-control" name="about_title"
                                           value="{!! getArrayData($data,'about_title') !!}" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Detail</label>
                                    <textarea class="form-control ckeditor" name="about_detail"
                                              rows="2" required>{!! getArrayData($data,'about_detail') !!}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-1"></div>

                            <div class="row col-sm-6 col-lg-5">
                                <div class="form-group cv-file col-lg-12">
                                    <div class="custom-file ">
                                        <label><b>Upload WhitePaper</b></label>
                                        <input type="file" class="form-control" placeholder="WhitePaper"
                                               value="{!! getArrayData($data,'about_white_paper') !!}" required name="about_white_paper">
                                    </div>
                                </div>

                                <div class="form-group cv-file col-lg-12">
                                    <label class="form-label">Youtube Video Code</label>
                                    <input type="text" class="form-control" name="about_video"
                                           value="{!! getArrayData($data,'about_video') !!}">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="1" name="sitemap_id">
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
