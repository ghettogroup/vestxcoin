@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/add', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">ROADMAP</h3>

                        <div class="row">
                            <div class="col-sm-6 col-lg-8" style="margin: auto">
                                <div class="form-group">
                                    <label class="form-label">Percent</label>
                                    <input type="text" class="form-control w-8 " name="custom_integer_1"
                                           value="" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Title</label>
                                    <input type="text" class="form-control" name="name"
                                           value="" required>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Detail</label>
                                    <textarea class="form-control ckeditor" name="detail"
                                              rows="2" required></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="6" name="sitemap_id">
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
