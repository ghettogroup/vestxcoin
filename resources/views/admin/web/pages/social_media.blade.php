@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url().'/edit', 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">SOCIAL MEDIA</h3>

                        <div class="col-sm-6 col-lg5">
                            <div class="form-group">
                                <label class="form-label">Facebook</label>
                                <input type="text" value="{!! getArrayData($data,'facebook') !!}"
                                       class="form-control" name="facebook">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Twitter</label>
                                <input type="text" value="{!! getArrayData($data,'twitter') !!}"
                                       class="form-control" name="twitter">
                            </div>
                            <div class="form-group">
                                <label class="form-label">İnstagram</label>
                                <input type="text" value="{!! getArrayData($data,'instagram') !!}"
                                       class="form-control" name="instagram">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Youtube</label>
                                <input type="text" value="{!! getArrayData($data,'youtube') !!}"
                                       class="form-control" name="youtube">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Github</label>
                                <input type="text" value="{!! getArrayData($data,'github') !!}"
                                       class="form-control" name="github">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Linkedin</label>
                                <input type="text" value="{!! getArrayData($data,'linkedin') !!}"
                                       class="form-control" name="linkedin">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Telegram</label>
                                <input type="text" value="{!! getArrayData($data,'linktelegramedin') !!}"
                                       class="form-control" name="telegram">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Discord</label>
                                <input type="text" value="{!! getArrayData($data,'linkedin') !!}"
                                       class="form-control" name="discord">
                            </div>
                        </div>
                        <input type="hidden" value="7" name="sitemap_id">
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
