@extends('admin.inc.app')
@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    {{ Form::open(array('url' => app('request')->url()."/add", 'method' => 'post', 'files' => true,'class' => 'card')) }}
                    <div class="card-body">
                        <h3 class="card-title">TEAMS</h3>
                    </div>
                    <div class="row col-lg-12">
                        <div class="col-sm-6 col-lg-3">
                            <span id="bottomFile" style="cursor: pointer;">
                                <label><b>Image</b></label>
                                <img src="{!! isset($data['image'] ) ? $data['image']  : "https://via.placeholder.com/100"!!}"
                                     alt="banner-photo" class="card p-1"
                                     style="width: 80%; margin-left: auto; margin-right: auto">
                            </span>
                            <div class="form-group" style="display: none">
                                <div class="custom-file ">
                                    <input type="file" class="form-control" id="bottom-file"
                                           name="image">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-sm-6 col-lg-8">
                            <div class="row">
                                <div class="custom-controls-stacked" style="margin: auto; ">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="status" value="1"
                                               checked>
                                        <span class="custom-control-label">Aktif</span>
                                    </label>
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="status" value="2">
                                        <span class="custom-control-label">Pasif</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Name</label>
                                <input type="text" id="name" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Title</label>
                                <input type="text" id="blogSlug" class="form-control" name="detail" required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="sitemap_id" value="5">
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#bottomFile').on('click', function () {
            $('#bottom-file').trigger('click');
        });
    </script>
@endpush