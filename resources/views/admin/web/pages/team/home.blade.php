@extends('admin.inc.app')

@section('content')
    <div class="my-3 my-md-5">
        <div class="container">
            <div class="page-header col-lg-12">
                <h1 class="page-title col-lg-10">
                    TEAM
                </h1>
                <div class="col-lg-2">
                    <a href="/admin/team/new" class="btn btn-info" style="padding-top: 0px; margin-top: -5px; padding-bottom: 0px;">Add New</a>
                </div>
            </div>
            <div class="row row-cards row-deck">
                @foreach($data as $value)
                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <a href="/admin/team/{!! $value->id !!}"><img class="card-img-top" src="{!! $value->image !!}"
                                         alt="{!! $value->detail->name !!}">
                        <div class="card-body d-flex flex-column">
                            <h4>{!! $value->detail->name !!}</h4>
                            <div class="text-muted">
                                {!! str_limit($value->detail->detail, 100) !!}
                            </div>
                        </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection