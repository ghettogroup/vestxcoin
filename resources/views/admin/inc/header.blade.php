<div class="header py-4">
    <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/admin/home">
                <img src="/assets/img/logo.png" class="header-brand-img" alt="php logo">
            </a>
        </div>
    </div>
</div>
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="/admin/about" class="dropdown-item "><i class="fa fa-comments"></i> About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/tools" class="dropdown-item "><i class="fe fe-codepen"></i> Tools</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/features" class="nav-link"><i class="fa fa-comments"></i> Features</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/blockchain-works" class="nav-link"><i class="fe fe-code"></i> Blockchain Works</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/team" class="nav-link"><i class="fa fa-users"></i> Best Teams</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/roadmap" class="nav-link"><i class="fa fa-road"></i> Roadmaps</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/social_media" class="nav-link"><i class="fa fa-share-square"></i> Social Media</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>