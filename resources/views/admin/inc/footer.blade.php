<footer class="footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12  text-center">
                Copyright © {!! date('Y') !!} All rights reserved.
            </div>
        </div>
    </div>
</footer>