<?php
/**
 * Created by PhpStorm.
 * User: alper
 * Date: 22.08.2018
 * Time: 19:51
 */


function getArrayData($array, $key, $default = null)
{
    if (array_key_exists($key, $array)) {
        return $array[$key];
    } else {
        if ($default != null) {
            return $default;
        } else {
            return null;
        }
    }
}

function getSitemapId($request) {
    $data = $request->segments()[1];
    if($data == 'about') $id = 1;
    elseif($data == 'tools') $id = 2;
    elseif($data == 'features') $id = 3;
    elseif($data == 'blockchain-works') $id = 4;
    elseif($data == 'team') $id = 5;
    elseif($data == 'roadmap') $id = 6;
    elseif($data == 'social_media') $id = 7;
    return $id;
}

//String'in ilk harfini büyük yapar
function ucfirst_turkish($str)
{
    $tmp = preg_split(
        "//u", $str, 2,
        PREG_SPLIT_NO_EMPTY
    );
    return mb_convert_case(
            str_replace("i", "İ", $tmp[0]),
            MB_CASE_TITLE, "UTF-8"
        ) .
        $tmp[1];
}

function key_sort_tr($data = [])
{
    if (env('APP_ENV') != 'local') {
        collator_sort(collator_create('tr_TR'), $data);
    }
    return $data;
}


//String'te ki kelimelerin ilk harflerini büyük yapar
function ucwords_turkish($str)
{
    return ltrim(mb_convert_case(str_replace(array(' I', ' ı', ' İ', ' i'), array(' I', ' I', ' İ', ' İ'), ' ' . $str), MB_CASE_TITLE, "UTF-8"));
}

//String'in hepsini belirtilen şekle dönüştürüyor.
function strto($to, $str)
{
    if ($to == 'lower') {
        return mb_strtolower(str_replace(array('I', 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç'), array('ı', 'ğ', 'ü', 'ş', 'i', 'ö', 'ç'), $str), 'utf-8');
    } elseif ($to == 'upper') {
        return mb_strtoupper(str_replace(array('ı', 'ğ', 'ü', 'ş', 'i', 'ö', 'ç'), array('I', 'Ğ', 'Ü', 'Ş', 'İ', 'Ö', 'Ç'), $str), 'utf-8');
    } else {
        trigger_error('Lütfen geçerli bir strto() parametresi giriniz.', E_USER_ERROR);
    }
}


function turkcetarih_formati($format, $datetime = 'now')
{
    $z = date($format, strtotime($datetime));
    $gun_dizi = array(
        'Monday' => 'Pazartesi',
        'Tuesday' => 'Salı',
        'Wednesday' => 'Çarşamba',
        'Thursday' => 'Perşembe',
        'Friday' => 'Cuma',
        'Saturday' => 'Cumartesi',
        'Sunday' => 'Pazar',
        'January' => 'Ocak',
        'February' => 'Şubat',
        'March' => 'Mart',
        'April' => 'Nisan',
        'May' => 'Mayıs',
        'June' => 'Haziran',
        'July' => 'Temmuz',
        'August' => 'Ağustos',
        'September' => 'Eylül',
        'October' => 'Ekim',
        'November' => 'Kasım',
        'December' => 'Aralık',
        'Mon' => 'Pts',
        'Tue' => 'Sal',
        'Wed' => 'Çar',
        'Thu' => 'Per',
        'Fri' => 'Cum',
        'Sat' => 'Cts',
        'Sun' => 'Paz',
        'Jan' => 'Oca',
        'Feb' => 'Şub',
        'Mar' => 'Mar',
        'Apr' => 'Nis',
        'Jun' => 'Haz',
        'Jul' => 'Tem',
        'Aug' => 'Ağu',
        'Sep' => 'Eyl',
        'Oct' => 'Eki',
        'Nov' => 'Kas',
        'Dec' => 'Ara',
    );
    foreach ($gun_dizi as $en => $tr) {
        $z = str_replace($en, $tr, $z);
    }
    if (strpos($z, 'Mayıs') !== false && strpos($format, 'F') === false) {
        $z = str_replace('Mayıs', 'May', $z);
    }
    return $z;
}
//Tarih Gün-Ay(String)-Yıl
function strtodate($date, $day = 1, $month = 1, $year = 1)
{
    $date = \Carbon\Carbon::parse($date);
    if ($day == 1) {
        $ret[] = 'j';
    }
    if ($month == 1) {
        $ret[] = "F";
    }
    if ($year == 1) {
        $ret[] = "Y";
    }
    $format = @implode(' ', $ret);

    return turkcetarih_formati($format,$date);
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}