<?php

namespace App\Http\Controllers;

use App\Models\Admin\Content;
use App\Models\Admin\Page;
use App\Models\Form;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['contents'] = Content::all();

        $data['features'] = Page::where('sitemap_id', 3)
            ->with('detail')
            ->get();

        $data['blockchain-works'] = Page::where('sitemap_id', 4)
            ->with('detail')
            ->get();

        $data['team'] = Page::where('sitemap_id', 5)
            ->with('detail')
            ->where('status', 1)
            ->get();

        $data['roadmap'] = Page::where('sitemap_id', 6)
            ->with('detail')
            ->get();

        $data['content'] = [];
        $data['social'] = [];
        foreach ($data['contents'] as $c) {
            if (strstr($c->key, 'twitter') || strstr($c->key, 'github') || strstr($c->key, 'instagram') || strstr($c->key, 'youtube') || strstr($c->key, 'linkedin') || strstr($c->key, 'facebook') || strstr($c->key, 'telegram') || strstr($c->key, 'discord'))
                $data['social'][$c->key] = $c->value;
            else
                $data['content'][$c->key] = $c->value;
        }
        return view('home', compact('data'));
    }

}

?>