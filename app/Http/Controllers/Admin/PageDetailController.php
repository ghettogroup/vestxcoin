<?php 

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PageDetailController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create(Request $request)
  {
      $arr = $request->segments()[1];
      return view('admin.web.pages.'.$arr.'.new');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $data = $request->all();
      unset($data['_token']);
      $file = $request->file('image');
      if($file != null) {
          $fileName = slugify($data['name'])."-".date("dmy-Gis").".".$file->getClientOriginalExtension();
          $file->storeAs('',$fileName);
          $data['image'] = Storage::disk('public')->url($fileName);
      }
      $page = Page::create($data);
      $page->detail()->create($data);
      return redirect()->to('/admin/'.$request->segments()[1]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id, Request $request)
  {
      $data = $request->all();
      unset($data['_token']);
      $file = $request->file('image');
      if($file != null) {
          $fileName = slugify($data['name'])."-".date("dmy-Gis").".".$file->getClientOriginalExtension();
          $file->storeAs('',$fileName);
          $data['image'] = Storage::disk('public')->url($fileName);
      }
      $page = Page::updateOrCreate(['id' => $id],$data);
      $page->detail()->updateOrCreate(['page_id' => $id],$data);
      return redirect()->back();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id, Request $request)
  {
      $arr = $request->segments()[1];
      $data = Page::with('detail')->find($id);

      return view('admin.web.pages.'.$arr.'.detail',compact('data'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id, Request $request)
  {
      $arr = $request->segments()[1];
      Page::find($id)->delete();
      return redirect()->to('/admin/'.$arr);
  }
  
}

?>