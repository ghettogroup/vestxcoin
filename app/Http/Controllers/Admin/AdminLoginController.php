<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;

class AdminLoginController extends Controller
{

    public function index()
    {
        return view('admin.web.login');
    }

    public function doLogin()
    {

        $userdata = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );
        if (Auth::attempt($userdata)) {
            return Redirect::to('/admin/about');
        } else {
            return Redirect::to('/admin')->withErrors('loginError', 'Mistake?');
        }
    }
}

?>