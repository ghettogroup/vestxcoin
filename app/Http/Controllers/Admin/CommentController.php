<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Comment;
use App\Models\Admin\Page;
use App\Models\Admin\PageDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $comments = Comment::orderBy('status')->get();
      $blogId = array_unique($comments->pluck('page_id')->toArray());
      $blogName = Page::where('sitemap_id',2)
          ->whereIn('id', $blogId)
          ->get()->pluck('detail.name','id')->toArray();
      return view('admin.web.pages.comment.home',compact('comments','blogName'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id, Request $request)
  {
      $data = $request->all();
      unset($data['_token']);
      Comment::updateOrCreate(['id' => $id],$data);
      return redirect()->back();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
      $data = Comment::find($id);
      return view('admin.web.pages.comment.detail',compact('data'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>