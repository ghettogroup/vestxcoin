<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Comment;
use App\Models\Admin\Page;
use App\Models\Admin\PageDetail;
use App\Models\FormDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $forms = FormDetail::all();
      foreach ($forms as $form) {
          $data[$form->id] = json_decode($form->data);
      }
      return view('admin.web.pages.forms.home',compact('data'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id, Request $request)
  {
      $data = $request->all();
      unset($data['_token']);
      FormDetail::updateOrCreate(['id' => $id],$data);
      return redirect()->back();
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
      $form = FormDetail::find($id);
      $data = json_decode($form->data);
      return view('admin.web.pages.forms.detail',compact('data'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>