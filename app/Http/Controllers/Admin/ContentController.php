<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $data = [];
        $id = getSitemapId($request);
        $banner = Content::where('sitemap_id', $id)->get();
        foreach ($banner as $b) {
            $data[$b->key] = $b->value;
        }
        return view('admin.web.pages.' . $request->segments()[1], compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $data = $request->all();
        $sitemap_id = $data['sitemap_id'];
        unset($data['_token']);
        unset($data['sitemap_id']);
        foreach ($data as $key => $d) {
            if($d != null) {
                $file = $request->file($key);
                if ($file != null) {
                    $fileName = $sitemap_id."-".$file->getClientOriginalName();
                    $file->storeAs('', $fileName);
                    Content::updateOrCreate(['key' => $key], ['value' => Storage::disk('public')->url($fileName), 'sitemap_id' => $sitemap_id]);
                }else {
                    Content::updateOrCreate(['key' => $key], ['value' => $d, 'sitemap_id' => $sitemap_id]);
                }
            }
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {

    }

}

?>