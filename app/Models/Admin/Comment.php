<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model 
{

    protected $table = 'comments';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('page_id', 'name', 'email', 'gender', 'message', 'ip_address', 'status', 'image');
    protected $visible = array('page_id', 'name', 'email', 'gender', 'message', 'ip_address', 'status', 'image');

    public function page()
    {
        return $this->hasOne('App\Models\Admin\Page');
    }

}