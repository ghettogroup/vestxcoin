<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PageDetail extends Model 
{

    protected $table = 'page_details';
    public $timestamps = true;
    protected $fillable = array('page_id', 'name', 'slug', 'detail', 'custom_text_1', 'custom_text_2');
    protected $visible = array('page_id', 'name', 'slug', 'detail', 'custom_text_1', 'custom_text_2');

    public function page()
    {
        return $this->hasOne('App\Models\Admin\Page');
    }

}