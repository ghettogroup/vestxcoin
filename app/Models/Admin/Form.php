<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model 
{

    protected $table = 'forms';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'slug', 'status');
    protected $visible = array('name', 'slug', 'status');

    public function details()
    {
        return $this->hasMany('App\Models\FormDetail');
    }

}