<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model 
{

    protected $table = 'contents';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('sitemap_id', 'page_id', 'key', 'value');
    protected $visible = array('sitemap_id', 'page_id', 'key', 'value');

    public function sitemap()
    {
        return $this->belongsTo('App\Models\Admin\Sitemap');
    }

}