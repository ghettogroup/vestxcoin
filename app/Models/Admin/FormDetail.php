<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormDetail extends Model 
{

    protected $table = 'form_details';
    public $timestamps = true;
    protected $fillable = array('form_id', 'email', 'data', 'ip', 'read');
    protected $visible = array('form_id', 'email', 'data', 'ip', 'read');

    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

}