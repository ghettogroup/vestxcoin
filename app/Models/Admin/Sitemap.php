<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Sitemap extends Model 
{

    protected $table = 'sitemaps';
    public $timestamps = true;
    protected $fillable = array('name', 'status');
    protected $visible = array('name', 'status');

    public function pages()
    {
        return $this->hasMany('App\Models\Admin\Page');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Admin\Content');
    }

}