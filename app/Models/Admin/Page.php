<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model 
{

    protected $table = 'pages';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('sitemap_id', 'status',  'image', 'views', 'custom_integer_1', 'custom_integer_2');
    protected $visible = array('sitemap_id', 'status', 'image', 'views', 'custom_integer_1', 'custom_integer_2');


    public function detail()
    {
        return $this->hasOne('App\Models\Admin\PageDetail');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Admin\Comment');
    }

    public function sitemap()
    {
        return $this->belongsTo('App\Models\Admin\Sitemap');
    }

}